// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2012 The Bitcoin developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
#ifndef BITCOIN_HASH_H
#define BITCOIN_HASH_H

#include "uint256.h"
#include "serialize.h"
#include "sync.h"


extern "C" {
#include "scrypt-jane/scrypt-jane.h"
}

#include <openssl/sha.h>
#include <openssl/ripemd.h>
#include <vector>
#include <stdio.h>

template<typename T1>

inline uint256 PoWHashScratchPad(const T1 pbegin, const T1 pend, scrypt_aligned_alloc* V1, scrypt_aligned_alloc* YX1, scrypt_aligned_alloc* V2, scrypt_aligned_alloc* YX2, scrypt_aligned_alloc* V3, scrypt_aligned_alloc* YX3)
{
	static unsigned char pblank[1];
	
	int HASHLENGTH=SHA512_DIGEST_LENGTH;
	unsigned char csoh[HASHLENGTH];
	
	

	SHA512((pbegin == pend ? pblank : (unsigned char*)&pbegin[0]), (pend - pbegin) * sizeof(pbegin[0]), (unsigned char*)&csoh);
	for(int i=0;i<12;i++){
		scryptscratchpad(csoh, HASHLENGTH, csoh, HASHLENGTH, 12, 1, 1, (unsigned char*)&csoh, HASHLENGTH, &(*V2), &(*YX2));
		for(int j=0;j<10;j++){
			scryptscratchpad(csoh, HASHLENGTH, csoh, HASHLENGTH, 8, 1, 1, (unsigned char*)&csoh, HASHLENGTH, &(*V1), &(*YX1));		
			SHA512(csoh, HASHLENGTH, (unsigned char*)&csoh);
		}
	}
	scryptscratchpad(csoh, HASHLENGTH, csoh, HASHLENGTH, 10, 8, 1, (unsigned char*)&csoh, HASHLENGTH, &(*V3), &(*YX3));
	uint256 hash2;
	SHA256((unsigned char*)&csoh, sizeof(csoh), (unsigned char*)&hash2);
	return hash2;

}

	//Each thread reserves its own memory for the hash
	static scrypt_aligned_alloc FV1;
	static scrypt_aligned_alloc FYX1;
	static scrypt_aligned_alloc FV2;
	static scrypt_aligned_alloc FYX2;
	static scrypt_aligned_alloc FV3;
	static scrypt_aligned_alloc FYX3;

	//Lock access to standard hashing thread so two threads don't try to use the same scratchpad
	static CCriticalSection lockScratchpad;

static bool initsp=false;
inline void initScratchPad(){
	FV1 = allocatePersisentMemoryV(8,1,1);
	FYX1 =allocatePersisentMemoryYX(8,1,1);
	FV2 = allocatePersisentMemoryV(12,1,1);
	FYX2 =allocatePersisentMemoryYX(12,1,1);
	FV3 = allocatePersisentMemoryV(10,8,1);
	FYX3 =allocatePersisentMemoryYX(10,8,1);
	initsp=true;
}

template<typename T1>

inline uint256 PoWHash(const T1 pbegin, const T1 pend)
{
	if(!initsp){
		initScratchPad();
	}
	static unsigned char pblank[1];
	
	int HASHLENGTH=SHA512_DIGEST_LENGTH;
	unsigned char csoh[HASHLENGTH];
	
{
	LOCK(lockScratchpad);
	SHA512((pbegin == pend ? pblank : (unsigned char*)&pbegin[0]), (pend - pbegin) * sizeof(pbegin[0]), (unsigned char*)&csoh);
	for(int i=0;i<12;i++){
		scryptscratchpad(csoh, HASHLENGTH, csoh, HASHLENGTH, 12, 1, 1, (unsigned char*)&csoh, HASHLENGTH, &FV2, &FYX2);
		for(int j=0;j<10;j++){
			scryptscratchpad(csoh, HASHLENGTH, csoh, HASHLENGTH, 8, 1, 1, (unsigned char*)&csoh, HASHLENGTH, &FV1, &FYX1);		
			SHA512(csoh, HASHLENGTH, (unsigned char*)&csoh);
		}
	}
	scryptscratchpad(csoh, HASHLENGTH, csoh, HASHLENGTH, 10, 8, 1, (unsigned char*)&csoh, HASHLENGTH, &FV3, &FYX3);
}
	uint256 hash2;
	SHA256((unsigned char*)&csoh, sizeof(csoh), (unsigned char*)&hash2);
	return hash2;

}

template<typename T1>
inline uint256 Hash(const T1 pbegin, const T1 pend)
{
    static unsigned char pblank[1];
    uint256 hash1;
    SHA256((pbegin == pend ? pblank : (unsigned char*)&pbegin[0]), (pend - pbegin) * sizeof(pbegin[0]), (unsigned char*)&hash1);
    uint256 hash2;
    SHA256((unsigned char*)&hash1, sizeof(hash1), (unsigned char*)&hash2);
    return hash2;
}

class CHashWriter
{
private:
    SHA256_CTX ctx;

public:
    int nType;
    int nVersion;

    void Init() {
        SHA256_Init(&ctx);
    }

    CHashWriter(int nTypeIn, int nVersionIn) : nType(nTypeIn), nVersion(nVersionIn) {
        Init();
    }

    CHashWriter& write(const char *pch, size_t size) {
        SHA256_Update(&ctx, pch, size);
        return (*this);
    }

    uint256 GetHash() {
        uint256 hash1;
        SHA256_Final((unsigned char*)&hash1, &ctx);
        uint256 hash2;
        SHA256((unsigned char*)&hash1, sizeof(hash1), (unsigned char*)&hash2);
        return hash2;
    }
    
    template<typename T>
    CHashWriter& operator<<(const T& obj) {
        // Serialize to this stream
        ::Serialize(*this, obj, nType, nVersion);
        return (*this);
    }
};


template<typename T1, typename T2>
inline uint256 Hash(const T1 p1begin, const T1 p1end,
                    const T2 p2begin, const T2 p2end)
{
    static unsigned char pblank[1];
    uint256 hash1;
    SHA256_CTX ctx;
    SHA256_Init(&ctx);
    SHA256_Update(&ctx, (p1begin == p1end ? pblank : (unsigned char*)&p1begin[0]), (p1end - p1begin) * sizeof(p1begin[0]));
    SHA256_Update(&ctx, (p2begin == p2end ? pblank : (unsigned char*)&p2begin[0]), (p2end - p2begin) * sizeof(p2begin[0]));
    SHA256_Final((unsigned char*)&hash1, &ctx);
    uint256 hash2;
    SHA256((unsigned char*)&hash1, sizeof(hash1), (unsigned char*)&hash2);

    return hash2;
}

template<typename T1, typename T2, typename T3>
inline uint256 Hash(const T1 p1begin, const T1 p1end,
                    const T2 p2begin, const T2 p2end,
                    const T3 p3begin, const T3 p3end)
{
    static unsigned char pblank[1];
    uint256 hash1;
    SHA256_CTX ctx;
    SHA256_Init(&ctx);
    SHA256_Update(&ctx, (p1begin == p1end ? pblank : (unsigned char*)&p1begin[0]), (p1end - p1begin) * sizeof(p1begin[0]));
    SHA256_Update(&ctx, (p2begin == p2end ? pblank : (unsigned char*)&p2begin[0]), (p2end - p2begin) * sizeof(p2begin[0]));
    SHA256_Update(&ctx, (p3begin == p3end ? pblank : (unsigned char*)&p3begin[0]), (p3end - p3begin) * sizeof(p3begin[0]));
    SHA256_Final((unsigned char*)&hash1, &ctx);
    uint256 hash2;
    SHA256((unsigned char*)&hash1, sizeof(hash1), (unsigned char*)&hash2);

    return hash2;
}

template<typename T>
uint256 SerializeHash(const T& obj, int nType=SER_GETHASH, int nVersion=PROTOCOL_VERSION)
{
    CHashWriter ss(nType, nVersion);
    ss << obj;
    return ss.GetHash();
}

template<typename T1>
inline uint160 Hash160(const T1 pbegin, const T1 pend)
{
    static unsigned char pblank[1];
    uint256 hash1;
    SHA256((pbegin == pend ? pblank : (unsigned char*)&pbegin[0]), (pend - pbegin) * sizeof(pbegin[0]), (unsigned char*)&hash1);
    uint160 hash2;
    RIPEMD160((unsigned char*)&hash1, sizeof(hash1), (unsigned char*)&hash2);


    return hash2;
}

inline uint160 Hash160(const std::vector<unsigned char>& vch)
{
    return Hash160(vch.begin(), vch.end());
}

unsigned int MurmurHash3(unsigned int nHashSeed, const std::vector<unsigned char>& vDataToHash);

#endif
