// Copyright (c) 2010 Satoshi Nakamoto
// Copyright (c) 2009-2012 The Bitcoin developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
#include "assert.h"

#include "chainparams.h"
#include "core.h"
#include "protocol.h"
#include "util.h"
#include "base58.h"
#include "scrypt-jane/scrypt-jane.h"

//
// Main network
//

unsigned int pnSeed[] =
{
   
};



uint256 verifyHashGenesisBlock;
uint256 verifyHashMerkleRoot;

//These are not in use
uint256 testNetVerifyHashGenesisBlock;//=uint256("80db92bba87fcbac630785d249017457c62b464e64d8f69cbbe6842482b0f4cf");
uint256 regTestVerifyHashGenesisBlock;//=uint256("d85efe72b8efab26cc6d762ddd345cf351c0fa7550815ba8193b20db2a31f9b7");


class CMainParams : public CChainParams {
public:
    CMainParams() {

	 string cointype="memorycoin";
	//string cointype="bitcoin";
	    
        // The message start string is designed to be unlikely to occur in normal data.
        // The characters are rarely used upper ASCII, not valid as UTF-8, and produce
        // a large 4-byte int at any alignment.
        pchMessageStart[0] = 0xf9;
        pchMessageStart[1] = 0xbe;
        pchMessageStart[2] = 0xb4;
        pchMessageStart[3] = 0xd9;
	
	const char* pszTimestamp;
	vector <unsigned char> scriptPubKeyAddress;
	uint64 scriptSigInt;
	uint64 coinnTime;
        unsigned int coinnBits;
        uint64 coinnNonce=1;
	int coinnVersion;    
	CBitcoinAddress address;

	//runHashingSpeedTests();

	if(cointype=="bitcoin"){
		//General
		/*
		nTargetTimespan = 14 * 24 * 60 * 60; // two weeks
		nTargetSpacing = 10 * 60; //every 600 seconds - 10 minute blocks
		nInterval = nTargetTimespan / nTargetSpacing;
		*/
		nSubsidyHalvingInterval = 210000;
		coinsInGenesis=50;
		
		//Genesis Block Stuff
		vAlertPubKey = ParseHex("04fc9702847840aaf195de8442ebecedf5b095cdbb9bc716bda9110971b28a49e0ead8564ff0db22209e0374782c093bb899692d524e9d6a6956e7c5ecbcd68284");
		scriptPubKeyAddress = ParseHex("04678afdb0fe5548271967f1a67130b7105cd6a828e03909a67962e0ea1f61deb649f6bc3f4cef38c4f35504e51ec112de5c384df7ba0b8d578a4c702b6bf11d5f");
		scriptSigInt = 486604799;
		nDefaultPort = 8333;
		nRPCPort = 8332;
		bnProofOfWorkLimit = CBigNum(~uint256(0) >> 32);
		pszTimestamp = "The Times 03/Jan/2009 Chancellor on brink of second bailout for banks";
		coinnTime    = 1231006505;
		coinnBits    = 0x1d00ffff;
		coinnVersion = 1;
		
		// Build the genesis block. Note that the output of the genesis coinbase cannot
		// be spent as it did not originally exist in the database.
		//
		// CBlock(hash=000000000019d6, ver=1, hashPrevBlock=00000000000000, hashMerkleRoot=4a5e1e, nTime=1231006505, nBits=1d00ffff, nNonce=2083236893, vtx=1)
		//   CTransaction(hash=4a5e1e, ver=1, vin.size=1, vout.size=1, nLockTime=0)
		//     CTxIn(COutPoint(000000, -1), coinbase 04ffff001d0104455468652054696d65732030332f4a616e2f32303039204368616e63656c6c6f72206f6e206272696e6b206f66207365636f6e64206261696c6f757420666f722062616e6b73)
		//     CTxOut(nValue=50.00000000, scriptPubKey=0x5F1DF16B2B704C8A578D0B)
		//   vMerkleTree: 4a5e1e
        
		//Precomputed hash and nonce.
		coinnNonce   = 2083236893;
		verifyHashGenesisBlock = uint256("0x000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f");
		verifyHashMerkleRoot= uint256("0x4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b");
	
	}else if(cointype=="memorycoin"){
        	//General
		/*
		nTargetTimespan = 60 * 60; // one hour
		nTargetSpacing = 6 * 60 ; // 360 seconds - six minute (6 minute blocks)
		nInterval = nTargetTimespan / nTargetSpacing; // retarget every hour?
		*/
		nSubsidyHalvingInterval = 1680; //Every Week - Note - we won't be halving, but decreasing by 5%
		coinsInGenesis=24;
		
		//Genesis Block Stuff
		//Broadcast alert signing key
		vAlertPubKey = ParseHex("04bbe9e4301e8d57d083f68920f3e64ea7f8e2610ddfed19f549c01302c1c70b49844c5eea47d10be99b60beca9a23ca07ce2b9e327be96ddf023571faa973c278");
		 
		//Address to send coins in genesis block to
		//address=CBitcoinAddress("MF4AANR2ByzkAFfuc6ysnFTW5wDJtGHqQ9");
		
		scriptSigInt = 486604799;
		nDefaultPort = 8555;
		nRPCPort = 8554;
		bnProofOfWorkLimit = CBigNum(~uint256(0) >> 5);
		pszTimestamp = "We have indeed at the moment little cause for pride";
		coinnTime    = 1375555555;
		coinnBits    = 0x2001fff0;
		coinnVersion = 1;

		scriptPubKeyAddress = ParseHex("04678afdb0fe5548271967f1a67130b7105cd6a828e03909a67962e0ea1f61deb649f6bc3f4cef38c4f35504e51ec112de5c384df7ba0b8d578a4c702b6bf11d5f");		

		//Precomputed hash.
		coinnNonce=765555555;
		verifyHashGenesisBlock=uint256("0122de45b781d8950cacd115119687c066efe8cdd25b437172dc2ff84e8a21f0");
		verifyHashMerkleRoot=uint256("c29e73012e83ff2d256a514b8df926af6a7ee0fb5e76b2daa5399df1f92e5b1c");
	}
	
	//Make the genensis block
	CTransaction txNew;
        txNew.vin.resize(1);
        txNew.vout.resize(1);
        txNew.vin[0].scriptSig = CScript() << scriptSigInt << CBigNum(4) << vector<unsigned char>((const unsigned char*)pszTimestamp, (const unsigned char*)pszTimestamp + strlen(pszTimestamp));
        txNew.vout[0].nValue = coinsInGenesis * COIN;
	txNew.vout[0].scriptPubKey = CScript() << scriptPubKeyAddress << OP_CHECKSIG;
        genesis.vtx.push_back(txNew);
        genesis.hashPrevBlock = 0;
        genesis.hashMerkleRoot = genesis.BuildMerkleTree();
        genesis.nVersion = coinnVersion;
        genesis.nTime    = coinnTime;
        genesis.nBits    = coinnBits;
        genesis.nNonce   = coinnNonce;
	hashGenesisBlock = genesis.ReCalculatePoWHash();
		
	assert(hashGenesisBlock == verifyHashGenesisBlock);
	assert(genesis.hashMerkleRoot == verifyHashMerkleRoot);
	

	if(cointype=="bitcoin"){
		vSeeds.push_back(CDNSSeedData("bitcoin.sipa.be", "seed.bitcoin.sipa.be"));
		vSeeds.push_back(CDNSSeedData("bluematt.me", "dnsseed.bluematt.me"));
		vSeeds.push_back(CDNSSeedData("dashjr.org", "dnsseed.bitcoin.dashjr.org"));
		vSeeds.push_back(CDNSSeedData("xf2.org", "bitseed.xf2.org"));
		base58Prefixes[PUBKEY_ADDRESS] = 0;
		base58Prefixes[SCRIPT_ADDRESS] = 5;
		base58Prefixes[SECRET_KEY] = 128;
	}else if(cointype=="memorycoin"){
		base58Prefixes[PUBKEY_ADDRESS] = 50; //letter M
		base58Prefixes[SCRIPT_ADDRESS] = 5;
		base58Prefixes[SECRET_KEY] = 178; //50+128
		vSeeds.push_back(CDNSSeedData("21stcenturymoneytalk.org", "meg-seed.21stcenturymoneytalk.org"));
	}

        
        // Convert the pnSeeds array into usable address objects.
        for (unsigned int i = 0; i < ARRAYLEN(pnSeed); i++)
        {
            // It'll only connect to one or two seed nodes because once it connects,
            // it'll get a pile of addresses with newer timestamps.
            // Seed nodes are given a random 'last seen time' of between one and two
            // weeks ago.
            const int64 nOneWeek = 7*24*60*60;
            struct in_addr ip;
            memcpy(&ip, &pnSeed[i], sizeof(ip));
            CAddress addr(CService(ip, GetDefaultPort()));
            addr.nTime = GetTime() - GetRand(nOneWeek) - nOneWeek;
            vFixedSeeds.push_back(addr);
        }
    }

    virtual const CBlock& GenesisBlock() const { return genesis; }
    virtual Network NetworkID() const { return CChainParams::MAIN; }

    virtual const vector<CAddress>& FixedSeeds() const {
        return vFixedSeeds;
    }
protected:
    CBlock genesis;
    vector<CAddress> vFixedSeeds;
};
static CMainParams mainParams;


//
// Testnet (v3)
//
class CTestNetParams : public CMainParams {
public:
    CTestNetParams() {
        // The message start string is designed to be unlikely to occur in normal data.
        // The characters are rarely used upper ASCII, not valid as UTF-8, and produce
        // a large 4-byte int at any alignment.
        pchMessageStart[0] = 0x0b;
        pchMessageStart[1] = 0x11;
        pchMessageStart[2] = 0x09;
        pchMessageStart[3] = 0x07;
        vAlertPubKey = ParseHex("04302390343f91cc401d56d68b123028bf52e5fca1939df127f63c6467cdf9c8e2c14b61104cf817d0b780da337893ecc4aaff1309e536162dabbdb45200ca2b0a");
        nDefaultPort = 18555;
        nRPCPort = 18554;
        strDataDir = "testnet3";

        // Modify the testnet genesis block so the timestamp is valid for a later start.
        genesis.nTime = 1296688602;
        genesis.nNonce = 414098458;
        hashGenesisBlock = genesis.GetPoWHash();
        
	    //printf("uint256 testNetVerifyHashGenesisBlock=uint256(\"%s\");\n",hashGenesisBlock.ToString().c_str());
	    //assert(hashGenesisBlock == uint256("000000000933ea01ad0ee984209779baaec3ced90fa3f408719526f8d77f4943"));
	    //assert(hashGenesisBlock == testNetVerifyHashGenesisBlock);
	    //3381182512c099836d830963803ad126c2fa09eb24a5ab89381fd3b0b21ad3eb
	
	
	
	    
        vFixedSeeds.clear();
        vSeeds.clear();
        vSeeds.push_back(CDNSSeedData("bitcoin.petertodd.org", "testnet-seed.bitcoin.petertodd.org"));
        vSeeds.push_back(CDNSSeedData("bluematt.me", "testnet-seed.bluematt.me"));

        base58Prefixes[PUBKEY_ADDRESS] = 111;
        base58Prefixes[SCRIPT_ADDRESS] = 196;
        base58Prefixes[SECRET_KEY] = 239;

    }
    virtual Network NetworkID() const { return CChainParams::TESTNET; }
};
static CTestNetParams testNetParams;


//
// Regression test
//
class CRegTestParams : public CTestNetParams {
public:
    CRegTestParams() {
        pchMessageStart[0] = 0xfa;
        pchMessageStart[1] = 0xbf;
        pchMessageStart[2] = 0xb5;
        pchMessageStart[3] = 0xda;
        nSubsidyHalvingInterval = 150;
        bnProofOfWorkLimit = CBigNum(~uint256(0) >> 1);
        genesis.nTime = 1296688602;
        genesis.nBits = 0x207fffff;
        genesis.nNonce = 2;
        hashGenesisBlock = genesis.GetPoWHash();
        nDefaultPort = 18444;
        strDataDir = "regtest";
	
	//printf("uint256 regTestVerifyHashGenesisBlock=uint256(\"%s\");\n",hashGenesisBlock.ToString().c_str());
        //assert(hashGenesisBlock == uint256("0x0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"));
	//assert(hashGenesisBlock == regTestVerifyHashGenesisBlock);

        vSeeds.clear();  // Regtest mode doesn't have any DNS seeds.

        base58Prefixes[PUBKEY_ADDRESS] = 0;
        base58Prefixes[SCRIPT_ADDRESS] = 5;
        base58Prefixes[SECRET_KEY] = 128;
    }

    virtual bool RequireRPCPassword() const { return false; }
    virtual Network NetworkID() const { return CChainParams::REGTEST; }
};
static CRegTestParams regTestParams;

static CChainParams *pCurrentParams = &mainParams;

const CChainParams &Params() {
    return *pCurrentParams;
}

void SelectParams(CChainParams::Network network) {
    switch (network) {
        case CChainParams::MAIN:
            pCurrentParams = &mainParams;
            break;
        case CChainParams::TESTNET:
            pCurrentParams = &testNetParams;
            break;
        case CChainParams::REGTEST:
            pCurrentParams = &regTestParams;
            break;
        default:
            assert(false && "Unimplemented network");
            return;
    }
}

bool SelectParamsFromCommandLine() {
    bool fRegTest = GetBoolArg("-regtest", false);
    bool fTestNet = GetBoolArg("-testnet", false);

    if (fTestNet && fRegTest) {
        return false;
    }

    if (fRegTest) {
        SelectParams(CChainParams::REGTEST);
    } else if (fTestNet) {
        SelectParams(CChainParams::TESTNET);
    } else {
        SelectParams(CChainParams::MAIN);
    }
    return true;
}


